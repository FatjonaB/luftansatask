import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Browser {//ben lancimin e driverit  dhe na drejton ne faqen e kaluar si parameter
        private  static WebDriver driver;
        public  static WebDriver initializeMyBrowser()
        {
            System.out.println("Initializing browser");
            System.setProperty("webdriver.chrome.driver", "C:\\selenium jars and drivers\\drivers\\chromedriver\\chromedriver.exe");
            driver=new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.get("https://demo.nopcommerce.com/");
            return  driver;

        }
}
