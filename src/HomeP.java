//import org.graalvm.compiler.debug.Assertions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HomeP extends MyPage {
    WebDriver driver;
    Actions action;
    WebDriverWait wait;

    public HomeP(WebDriver driver) {
        super(driver);
        this.driver = driver;
        action = new Actions(driver);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //js.executeScript("arguments[0].click();",goToCart);
        wait = new WebDriverWait(driver, 20);
    }

    //gjetja e elementit LoginMenu
    @FindBy(xpath = "//a[@class=\"ico-login\"]")
    private WebElement loginMenu;
    @FindBy(xpath = "//ul[@class=\"top-menu notmobile\"]//li[1]//a[1][text()=\"Computers \"]")
    private WebElement computersMenu;
    @FindBy(xpath = "//span[@class=\"cart-label\"]")
    private WebElement shoppingCartMenu;
    @FindBy(xpath = "//ul[1]/li[1]/ul/li[2]/a")
    private WebElement notebooks;
    @FindBy(xpath = "//div[@class=\"page-title\"]//h1")
    private WebElement containsNotebooks;
    @FindBy(xpath = "//button[@class=\"button-1 cart-button\"]")
    private WebElement goToCart;


    //klikoni ne loginMenu
    public void clickLoginMenu() {
        loginMenu.click();

    }

    public void hoverComputersMenu() {

        action.moveToElement(computersMenu);
        action.perform();
        clickNotebooks();
    }

    public void hoverShoppingCartMenu() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@class=\"content\"]")));
        action.moveToElement(shoppingCartMenu);
        action.perform();
        checkGoToCartDisplayed();
    }

    public void clickNotebooks() {
        //To mouseover on sub menu
        action.moveToElement(notebooks);

        //build()- used to compile all the actions into a single step
        action.click().build().perform();
    }

    //3.  Verify that we have navigate to Notebooks Page
    public void verifyNoteBookPage() {
        Assert.assertEquals("Notebooks", containsNotebooks.getText());
    }
    public void checkGoToCartDisplayed() {
        if (goToCart.isDisplayed()) {
            //click button if displayed
            goToCart.click();
        }
    }


}
