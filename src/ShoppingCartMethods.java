import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartMethods extends MyPage {
    WebDriver driver;
    WebDriverWait wait;

    public ShoppingCartMethods(WebDriver driver) {
        super(driver);
        this.driver=driver;
        wait=new WebDriverWait(driver,20);
    }


    @FindBy(xpath = "//button[@class=\"remove-btn\"]")
    private  List<WebElement> removeFromCartButtons;

    @FindBy(xpath = "//button[@class='button-2 update-cart-button']")
    private  WebElement updateCartButton;
    @FindBy(xpath = "//button[@class='button-2 continue-shopping-button']")
    private WebElement continueShopping;
    @FindBy(xpath = "//a[@class='estimate-shipping-button']")
    public WebElement estimateShoppingCart;
    @FindBy(xpath = "//div[@class=\"no-data\"]")
    private WebElement textAfterRemovingItems;
    @FindBy(xpath = "//span[@class=\"product-subtotal\"]")
    private List<WebElement> priceOfProduct;
    @FindBy(xpath = "//tr[@class=\"order-total\"]//strong")
    private WebElement totalPriceExpected;
    @FindBy(xpath = "//div[@class=\"page-title\"]//h1")
    private  WebElement navigateShoppingCart;

    @FindBy(xpath = "//div[@class=\"common-buttons\"]//button")
    private List<WebElement>buttonsDisplayed;
    public static final String GO_TO_CART_BUTTON_CSS = "button[class='button-1 cart-button']";
    @FindBy(xpath = "//table[@class='cart-total']//tr[@class='order-total']/td[@class='cart-total-right']//strong")
    public WebElement totalValue;
    @FindBy(xpath = "//td[@class='subtotal']//span[@class='product-subtotal']")
    public List<WebElement> subTotalPrice;
    @FindBy(xpath = "//button[@class='button-2 product-box-add-to-cart-button']")
    public List<WebElement> addButtonsToCart;
    public static final String SUCCESS_MSG_XPATH = "//div[@class='bar-notification success']";
    @FindBy(xpath = SUCCESS_MSG_XPATH)
    public WebElement succesMsg;
    public static final String SHOPPING_CART_SUCCESS_MESSAGE = "The product has been added to your shopping cart";
    @FindBy(xpath = "//button[@class='button-2 add-to-wishlist-button']")
    public List<WebElement> addButtonsToWishList;
    public static final String WISHLIST_SUCCESS_MESSAGE = "The product has been added to your wishlist";
    @FindBy(xpath = "//*[@class=\"content\"]")
    private  WebElement addedToCart;
    @FindBy(xpath = "//*[@class=\"content\"]")
    private  WebElement addedToWishList;
    @FindBy(className = "wishlist-qty")
    private WebElement wishlist_qty;
    @FindBy(className = "cart-qty")
    private WebElement cart_qty;

    //Add the second and the third item on wishList
    public void addItemToShoppingCart(final int itemNumber) {
        for (int i = 0; i < addButtonsToCart.size(); i++) {
            if (i + 1 == itemNumber) {
                addButtonsToCart.get(i).click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SUCCESS_MSG_XPATH)));
                final String message = succesMsg.getText();
                Assert.assertEquals(message, SHOPPING_CART_SUCCESS_MESSAGE);
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(SUCCESS_MSG_XPATH)));
                break;
            }
        }
    }
    public void addedToWishList()
    {
        String s=addedToWishList.getText();
        Assert.assertEquals("The product has been added to your ",s);

    }
    //Add the fourth, fifth and sixth item on Shopping Cart
    public void addItemToWishlist(final int itemNumber) {
        for (int i = 0; i < addButtonsToWishList.size(); i++) {
            if (i + 1 == itemNumber) {
                addButtonsToWishList.get(i).click();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SUCCESS_MSG_XPATH)));
                final String message = succesMsg.getText();
                Assert.assertEquals(message, WISHLIST_SUCCESS_MESSAGE);
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(SUCCESS_MSG_XPATH)));
                break;
            }
        }
    }
    //Verify that after every item added a notification with text : The product has been added to your shopping cart
    public void addedToCart()
    {
        String s=addedToCart.getText();
        Assert.assertEquals("The product has been added to your shopping cart ",s);
    }
    public void wishListDispay2()
    {
        Assert.assertEquals("(2)",wishlist_qty.getText());
    }
    public void cartDisplay3()
    {
        Assert.assertEquals("(3)",cart_qty.getText());
    }
    public void verifyNavigateShoppingCart()
    {
        Assert.assertEquals("Shopping cart",navigateShoppingCart.getText());
    }


    public void sumSubTotalPrice()
    {
        String valueTotal = totalValue.getText().substring(1, totalValue.getText().length() - 1).replace(",", "");
        double valueTotalPrice = Double.parseDouble(valueTotal);

        double sum = 0;
        for(final WebElement element:subTotalPrice)
        {
            String value = element.getText().substring(1, element.getText().length() - 1).replace(",", "");
            double price = Double.parseDouble(value);
            sum = sum + price;
        }
        //check for equality
        Assert.assertEquals(sum,valueTotalPrice,0);

    }


    public void buttonsAreDisplayed3()
    {

        Assert.assertTrue("Update Button is not displayed",updateCartButton.isDisplayed());
        Assert.assertTrue("Continue Button is not displayed",continueShopping.isDisplayed());
        Assert.assertTrue("Estimate Button is not displayed",estimateShoppingCart.isDisplayed());
    }
    public void  removeItemFromCart() throws InterruptedException {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(GO_TO_CART_BUTTON_CSS)));
        int nrOfElements=removeFromCartButtons.size();
        while(nrOfElements>0)
        {
            removeFromCartButtons.get(0).click();//delete first item
            nrOfElements--;
            if(nrOfElements>0)
            {
                int actualSize=removeFromCartButtons.size();
                Assert.assertEquals(actualSize,nrOfElements);
            }
        }
        Assert.assertEquals(nrOfElements,0);
    }








}
