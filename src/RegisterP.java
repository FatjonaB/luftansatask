import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RegisterP extends MyPage{
    WebDriver driver;
    public RegisterP(WebDriver driver) {
        super(driver);
        this.driver=driver;
    }
    //merr gjinine
    @FindBy(css = "input[name='Gender']")
    public List<WebElement> genderOptions;

    //merr emrin
    @FindBy(xpath = " //input[@id=\"FirstName\"]")
    private  WebElement firstName;

    //merr mbiemrin
    @FindBy(xpath = " //input[@id=\"LastName\"]")
    private WebElement lastName;
    //merr ditelindjen
    //dita
    @FindBy(xpath = "//select[@name=\"DateOfBirthDay\"]")
    private WebElement  day;
    //muaji
    @FindBy(xpath = " //select[@name=\"DateOfBirthMonth\"]")
    private WebElement month;
    //viti
    @FindBy(xpath = " //select[@name=\"DateOfBirthYear\"]")
    private WebElement year;

    //merr email
    @FindBy(xpath = "//input[@name=\"Email\"]")
    private WebElement email;
    //merr detajet e kompanise
    @FindBy(xpath = "//input[@name=\"Company\"]")
    private WebElement companyName;
    //merr password
    @FindBy(xpath = "//input[@name=\"Password\"]")
    private WebElement paswword;
    //merr resetPaswword
    @FindBy(xpath = "//input[@name=\"ConfirmPassword\"]")
    private WebElement confirmPaswword;
    //registerButton
    @FindBy(xpath = "//button[@name=\"register-button\"]")
    private WebElement register;
    //teksti pasi regjistrohemi me sukses
    @FindBy(xpath = "//div[@class=\"page-body\"]//div[1]")
    private WebElement completeRegistration;
    //LogOut
    @FindBy(xpath = "//a[text()=\"Log out\"]")
    private WebElement logOut;
    @FindBy(xpath = " //select[@name=\"DateOfBirthMonth\"]")
    private WebElement monthOfBirthSelect;
    //zgjidhni gjinine
    String  emri,mbiemri,dita,muaji,viti,emaili,kompania;


    public void radioButtonGenderClick(final String value)
    {
        for(final WebElement el:genderOptions)
        {
            if(el.getAttribute("value").equalsIgnoreCase(value))
            {
                el.click();
                break;
            }
        }
    }

    //ploteso emrin
    public void plotesoEmrin()
    {
        firstName.sendKeys("fatjona");

    }
    public String getEmri()
    {
        emri=firstName.getAttribute("value");
        return emri;
    }
        public void plotesoMbiemrin()
    {
        lastName.sendKeys("bucpapaj");
    }
    public String getMbiemri()
    {
        mbiemri=lastName.getAttribute("value");
        return mbiemri;
    }
//navigimi ne day
    public void zgjidhDiten()
    {
        new Select(day).selectByVisibleText("12");
    }
    public String getDay()
    {
        dita=new Select(day).getFirstSelectedOption().getText();
        return dita;
    }
    //zgjidh muajin
    public void zgjidhMuajin(final String valueMonth)
    {
        final Select month = new Select(monthOfBirthSelect);
        month.selectByVisibleText(valueMonth);
    }

    public String getMonth()
    {
        muaji=new Select(month).getFirstSelectedOption().getText();
        return muaji;
    }
    //zgjidh vitin
    public void zgjidhVitin()
    {
        new Select(year).selectByVisibleText("2021");
    }
    public String getYear()
    {
        viti=new Select(year).getFirstSelectedOption().getText();
        return viti;
    }
    public  void plotesoEmail()
    {
        email.sendKeys("fat@yahoo.com");
    }
    public String getEmail()
    {
        emaili=email.getAttribute("value");
        return emaili;
    }
    public void plotesoEmrinEKompanise()
    {
        companyName.sendKeys("info");
    }
    public String  getEmriKomapnise()
    {
        kompania=companyName.getAttribute("value");
        return kompania;
    }
    public  void plotesoPassword()
    {
        paswword.sendKeys("12345678");

    }
    public void plotesoConfirmPassword()
    {
        confirmPaswword.sendKeys("12345678");
    }
    public void klikoLogOut()
    {
        logOut.click();
    }
    public  void regjistrohu()
    {
        radioButtonGenderClick("M");
        plotesoEmrin();
        plotesoMbiemrin();
        zgjidhDiten();
        zgjidhMuajin("January");
        zgjidhVitin();
        plotesoEmail();
        plotesoEmrinEKompanise();
        plotesoPassword();
        plotesoConfirmPassword();
        register.click();
        //verifikojme nqs regjistrimi u krye me sukses
        Assert.assertTrue(completeRegistration.getText().contains("Your registration completed"));
        klikoLogOut();
    }

}
