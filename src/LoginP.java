import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Driver;

public class LoginP extends MyPage{
    WebDriver driver;
    WebDriverWait wait;
    public LoginP(WebDriver driver) {
        super(driver);
        this.driver=driver;
        wait=new WebDriverWait(driver,20);
    }
    @FindBy(xpath = "//button[text()=\"Register\"]")
    private WebElement register;
    @FindBy(xpath = "//input[@name=\"Email\"]")
    private  WebElement email;
    @FindBy(xpath = "//input[@name=\"Password\"]")
    private  WebElement password;
    @FindBy(xpath = "//button[text()=\"Log in\"]")
    private WebElement logIn;
    @FindBy(xpath = "//div[@class=\"topic-block\"]//h2")
    private  WebElement welcome;
    @FindBy(xpath ="//a[@class=\"ico-logout\"]")
    private WebElement displayedLogoutMenu;
    @FindBy(xpath = "//a[text()=\"Log out\"]")
    private WebElement logout;

    public  static  final String LOGIN_ACCOUNT_BTN_XPATH ="//button[text()=\"Log in\"]";
    //kliko ne butonin register
    public void clickRegister(){
        register.click();
        //printo titullin e faqes pak klikimit te regster
    }

    public void login()
    {

        email.sendKeys("fat@yahoo.com");
        password.sendKeys("12345678");
        logIn.click();
        //verifikojme qe login u krye me sukses
        loginSucess();
    }
    public  void loginSucess()
    {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(LOGIN_ACCOUNT_BTN_XPATH)));
        Assert.assertTrue(welcome.getText().contains("Welcome to our store"));
        Assert.assertTrue(displayedLogoutMenu.getText().contains("Log out"));
    }
    public  void logout()
    {
        if(logout.isDisplayed())
        {
            logout.click();
        }
        else
        {
            System.out.println("cant logout ,because you dont have an account!");
        }

    }
}
