import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomerP extends  MyPage{
    WebDriver driver;
    public CustomerP(WebDriver driver){
        super(driver);
        this.driver=driver;
    }
    //myAccount
    @FindBy(xpath = "//a[@class=\"ico-account\"]")
    private  WebElement myAccount;
    @FindBy(xpath = "//div[@class=\"page-title\"]//h1")
    private WebElement myAccountPage;

    @FindBy(xpath = "//input[@id=\"gender-male\"]")
    private WebElement male;
    @FindBy(xpath = "//input[@id=\"gender-female\"]")
    private WebElement female;
    //merr emrin
    @FindBy(xpath = " //input[@id=\"FirstName\"]")
    private  WebElement firstName;

    //merr mbiemrin
    @FindBy(xpath = " //input[@id=\"LastName\"]")
    private WebElement lastName;
    //merr ditelindjen
    //dita
    @FindBy(xpath = "//select[@name=\"DateOfBirthDay\"]")
    private WebElement  day;
    //muaji
    @FindBy(xpath = "//select[@name=\"DateOfBirthMonth\"]")
    private WebElement month;
    //viti
    @FindBy(xpath = "//select[@name=\"DateOfBirthYear\"]")
    private WebElement year;

    //merr email
    @FindBy(xpath = "//input[@name=\"Email\"]")
    private WebElement email;
    //merr detajet e kompanise
    @FindBy(xpath = "//input[@name=\"Company\"]")
    private WebElement companyName;


    public  void  clickMyAccountMenu(){
        myAccount.click();
    }
    public void confirmAccountPage()
    {
        Assert.assertTrue(myAccountPage.getText().contains("My account - Customer info"));
    }

    public void verifyBirthMonth(final String expected) {
        final Select muaji = new Select(month);
        final String value = muaji.getFirstSelectedOption().getText();
        Assert.assertEquals(value, expected);
    }
    public void checkSaktesine()
    {

        RegisterP registerP=new RegisterP(driver);
        Assert.assertEquals(registerP.getEmri(),firstName.getAttribute("value"));
        Assert.assertEquals(registerP.getMbiemri(),lastName.getAttribute("value"));
        Assert.assertEquals(registerP.getDay(),day.getAttribute("value"));
        verifyBirthMonth("January");
        Assert.assertEquals(registerP.getYear(),year.getAttribute("value"));
        Assert.assertEquals(registerP.getEmail(),email.getAttribute("value"));
        Assert.assertEquals(registerP.getEmriKomapnise(),companyName.getAttribute("value"));
    }
}
