import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import javax.sound.midi.SysexMessage;
@FixMethodOrder(MethodSorters.DEFAULT)
public class TestFrame {
    WebDriver driver=Browser.initializeMyBrowser();

    @Test
    public void registerLogInLogOutTest() throws InterruptedException {

        HomeP hp=new HomeP(driver);

        hp.clickLoginMenu();
        LoginP lp=new LoginP(driver);
        lp.clickRegister();
        System.out.println("titulli i faqes pas klikimit te butonit Register eshte:"+driver.getTitle());
        RegisterP rp=new RegisterP(driver);
        rp.regjistrohu();
        hp.clickLoginMenu();
        lp.login();
        lp.loginSucess();

        CustomerP cp=new CustomerP(driver);
        cp.clickMyAccountMenu();
        cp.confirmAccountPage();
        cp.checkSaktesine();
        lp.logout();


    }

@Test
  public  void cartAndWishListTest() throws InterruptedException {
  HomeP hp=new HomeP(driver);
  hp.hoverComputersMenu();


        NotebooksP notebooksP=new NotebooksP(driver);
        notebooksP.chooseDisplay9();
        notebooksP.checkIf6Elements();
        notebooksP.check16GB();
        notebooksP.uncheck16GB();
        notebooksP.checkIf6Elements();
        Thread.sleep(6000);

        ShoppingCartMethods shoppingCartMethods=new ShoppingCartMethods(driver);
        shoppingCartMethods.addItemToWishlist(2);
        shoppingCartMethods.addItemToWishlist(3);
        shoppingCartMethods.wishListDispay2();
        Thread.sleep(3000);
        shoppingCartMethods.addItemToShoppingCart(4);
        shoppingCartMethods.addItemToShoppingCart(5);
        shoppingCartMethods.addItemToShoppingCart(6);
        shoppingCartMethods.cartDisplay3();
        hp.hoverShoppingCartMenu();
        shoppingCartMethods.buttonsAreDisplayed3();
        shoppingCartMethods.sumSubTotalPrice();
        shoppingCartMethods.verifyNavigateShoppingCart();
        shoppingCartMethods.removeItemFromCart();


}
@After
    public void closeDriver()
    {
        driver.quit();
    }
}
