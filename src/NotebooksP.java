import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class NotebooksP extends MyPage {
    WebDriver driver;
    WebDriverWait wait;
    int count1=0,count2=0;
    public NotebooksP(WebDriver driver) {
        super(driver);
        this.driver=driver;
        wait=new WebDriverWait(driver,20);
    }
    @FindBy(xpath = "//select[@id=\"products-pagesize\"]")
    private  WebElement dispaly9;
    @FindBy(xpath="//div[@class=\"page category-page\"]//img")
    private List<WebElement>notebookElements;
    @FindBy(xpath = "//label[text()=\" 16 GB \"]")
    private WebElement gb16;
    @FindBy(xpath = "//div[@class=\"page-body\"]//img")
    private List<WebElement> filteredgb16;

    public void chooseDisplay9()
    {
        Select disp9 = new Select(driver.findElement(By.xpath("//*[@id=\"products-pagesize\"]")));
        disp9.selectByVisibleText("9");
    }
        public void checkIf6Elements()
        {
           count1= notebookElements.size();
           Assert.assertEquals(6,count1);
        }
    //On Filter by attributes check 16GB
    public  void check16GB()
    {
        gb16.click();
        //checkIf1Element();
    }
    //Verify that only 1 item is displayed
    public void checkIf1Element()
    {
        count2= filteredgb16.size();
        Assert.assertEquals(1,count2);
    }
    //Uncheck the 16GB checkbox
    public void uncheck16GB(){
        if(!gb16.isSelected())
        {
            gb16.click();
        }

    }

}
